<?php
/**
 * Curse Inc.
 * Hydralytics
 * Google Analytics Class
 *
 * @author		Alexia E. Smith
 * @copyright	(c) 2014 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		Hydralytics
 * @link		https://gitlab.com/hydrawiki
 *
 **/

namespace Hydralytics;

class GoogleAnalytics {
	/**
	 * Google_Client Class
	 *
	 * @var		object
	 */
	private $client;

	/**
	 * Google_Service_Analytics Class
	 *
	 * @var		object
	 */
	private $analytics;

	/**
	 * Google Analytics Information(View ID, Account ID, Etcetera...)
	 *
	 * @var		array
	 */
	private $gaInfo;

	/**
	 * Default metrics to handle for monthly totals.
	 *
	 * @var		array
	 */
	private $metrics = [
		'ga:sessions',
		'ga:pageviews',
		'ga:users',
		'ga:newUsers'
	];

	/**
	 * Main Constructor
	 *
	 * @access	public
	 * @return	void
	 */
	public function __construct() {
		if (file_exists(dirname(__DIR__).'/.ga-private.json')) {
			putenv('GOOGLE_APPLICATION_CREDENTIALS='.dirname(__DIR__).'/.ga-private.json');
		}
		$this->client = new \Google_Client();
		$this->client->setApplicationName("Hydralytics");
		$this->client->useApplicationDefaultCredentials();
		$this->client->addScope(\Google_Service_Analytics::ANALYTICS);
		$this->client->addScope(\Google_Service_Analytics::ANALYTICS_EDIT);
		$this->analytics = new \Google_Service_Analytics($this->client);

		$this->redis = \RedisCache::getClient('cache');
	}

	/**
	 * Loads GA Information from provided site keys and domains.
	 *
	 * @access	public
	 * @global 	string $wgAnalyticsOverrideAccountID to override default account id
	 * @param	array	Array of $siteKey => $domain.
	 * @return	boolean	Success
	 */
	public function loadGAInfo($sites) {
		global $wgAnalyticsOverrideAccountID;
		$gaInfo = [];

		foreach ($sites as $siteKey => $propertyTag) {
			$_gaInfo = unserialize($this->redis->get('hydralytics:gaInfo:'.$siteKey), ['allowed_classes' => false]);
			if (!is_array($_gaInfo) || !$_gaInfo['view_id'] > 0) {
				try {
					$this->rateLimitQueue();
					list(, $accountId) = explode('-', $propertyTag);
					if ($wgAnalyticsOverrideAccountID) {
						$accountId = (string) $wgAnalyticsOverrideAccountID;
					}
					$property = $this->analytics->management_webproperties->get($accountId, $propertyTag);
				} catch ( \Exception $e ) {
					wfDebug(__METHOD__." - Could not load property due to: ".$e->getMessage());
					return false;
				}

				$gaInfo[$siteKey] = [
					'view_id'			=> $property->id,
					'profile_id'		=> $property->defaultProfileId,
					'account_id'		=> $property->accountId,
					'web_property_id'	=> $property->internalWebPropertyId,
					'name'				=> $property->name,
					'website_url'		=> $property->websiteUrl,
				];

				$this->redis->setEx('hydralytics:gaInfo:'.$siteKey, 604800, serialize($gaInfo[$siteKey]));
			} else {
				$gaInfo[$siteKey] = $_gaInfo;
			}
		}

		$this->gaInfo = $gaInfo;
		unset($gaInfo);

		return true;
	}

	/**
	 * Get GA Information based on a site key.
	 *
	 * @access	public
	 * @param	string	Site Key to look up.
	 * @return	mixed	GA Info or false if it does not exist.
	 */
	public function getGAInfo($siteKey) {
		return (array_key_exists($siteKey, $this->gaInfo) ? $this->gaInfo[$siteKey] : false);
	}

	/**
	 * Get daily data for the specified range.
	 *
	 * @access	public
	 * @param	integer	Start Timestamp - Unix Format
	 * @param	integer	End Timestamp - Unix Format
	 * @param	string	Site key to look up.
	 * @return	mixed	Array of organized data or false on error.
	 */
	public function getDailyData($startTimestamp, $endTimestamp, $siteKey) {
		if ($this->validateParameters($startTimestamp, $endTimestamp, $siteKey) === false) {
			return ['success' => false, 'error' => 'invalid_parameters'];
		}

		try {
			$this->rateLimitQueue();
			$gaData = $this->analytics->data_ga->get('ga:'.$this->gaInfo[$siteKey]['profile_id'], gmdate('Y-m-d', $startTimestamp), gmdate('Y-m-d', $endTimestamp), implode(',', $this->metrics), ['dimensions' => 'ga:date']);

			if ($gaData instanceof \Google_Service_Analytics_GaData) {
				if (is_array($gaData->rows)) {
					foreach ($gaData->rows as $data) {
						$dateNumbers = str_split($data[0]);
						$date = sprintf('%d%d%d%d-%d%d-%d%d', $dateNumbers[0], $dateNumbers[1], $dateNumbers[2], $dateNumbers[3], $dateNumbers[4], $dateNumbers[5], $dateNumbers[6], $dateNumbers[7]);
						$dailyData[$this->metrics[0]][$date] = $data[1];
						$dailyData[$this->metrics[1]][$date] = $data[2];
						$dailyData[$this->metrics[2]][$date] = $data[2];
					}
				}

				if (is_array($dailyData) && count($dailyData)) {
					foreach ($dailyData as $metric => $dates) {
						$previousNumber = null;
						foreach ($dates as $date => $number) {
							if ($previousNumber !== null) {
								$dailyData[$metric.'_delta'][$date] = $number - $previousNumber;
							}
							$previousNumber = $number;
						}
					}
				}
			}
			return ['success' => true, 'data' => $dailyData];
		} catch (\Google_Service_Exception $e) {
			return ['success' => false, 'error' => $e];
		}
	}

	/**
	 * Get thirty day data for the specified range.
	 *
	 * @access	public
	 * @param	integer	Start Timestamp - Unix Format
	 * @param	integer	End Timestamp - Unix Format
	 * @param	string	Site key to look up.
	 * @return	mixed	Array of organized data or false on error.
	 */
	public function getTotalsData($startTimestamp, $endTimestamp, $siteKey) {
		if ($this->validateParameters($startTimestamp, $endTimestamp, $siteKey) === false) {
			return ['success' => false, 'error' => 'invalid_parameters'];
		}

		try {
			$this->rateLimitQueue();
			$gaData = $this->analytics->data_ga->get('ga:'.$this->gaInfo[$siteKey]['profile_id'], gmdate('Y-m-d', $startTimestamp), gmdate('Y-m-d', $endTimestamp), implode(',', $this->metrics));

			if ($gaData instanceof \Google_Service_Analytics_GaData) {
				if (is_array($gaData->totalsForAllResults)) {
					$totalsData = $gaData->totalsForAllResults;
				}
			}
			return ['success' => true, 'data' => $totalsData];
		} catch (\Google_Service_Exception $e) {
			return ['success' => false, 'error' => $e];
		}
	}

	/**
	 * Gets a list of geographic data based on the metrics.
	 *
	 * @access	public
	 * @param	integer	Start Timestamp - Unix Format
	 * @param	integer	End Timestamp - Unix Format
	 * @param	string	Site key to look up.
	 * @param	integer	Maximum number of results.
	 * @return	mixed	Array of organized data or false on error.
	 */
	public function getGeographicData($startTimestamp, $endTimestamp, $siteKey, $maxResults = 10) {
		if ($this->validateParameters($startTimestamp, $endTimestamp, $siteKey) === false) {
			return false;
		}

		$countryData = [];
		try {
			$this->rateLimitQueue();
			$gaData = $this->analytics->data_ga->get(
				'ga:'.$this->gaInfo[$siteKey]['profile_id'],
				gmdate('Y-m-d', $startTimestamp),
				gmdate('Y-m-d', $endTimestamp),
				implode(',', $this->metrics),
				[
					'dimensions' => 'ga:country',
					'max-results' => intval($maxResults),
					'sort' => '-ga:pageviews'
				]
			);

			if ($gaData instanceof \Google_Service_Analytics_GaData) {
				if (is_array($gaData->rows)) {
					foreach ($gaData->rows as $data) {
						$country = $data[0];
						$countryData[$this->metrics[0]][$country] = $data[1];
						$countryData[$this->metrics[1]][$country] = $data[2];
					}
				}
			}
		} catch (\Google_Service_Exception $e) {
			wfDebug("We could not retrieve geographic data due to... ".$e->getMessage()."\n");
			return false;
		}

		return $countryData;
	}

	/**
	 * Get a list of top popular pages.
	 *
	 * @access	public
	 * @param	integer	Start Timestamp - Unix Format
	 * @param	integer	End Timestamp - Unix Format
	 * @param	string	Site key to look up.
	 * @param	integer	[Optional] Maximum number of results.
	 * @param	string	[Optional] GA metric filter string.
	 * @return	mixed	Array of organized data or false on error.
	 */
	public function getPopularPages($startTimestamp, $endTimestamp, $siteKey, $maxResults = 10, $filter = null) {
		if ($this->validateParameters($startTimestamp, $endTimestamp, $siteKey) === false) {
			return false;
		}

		$pageData = [];
		try {
			$this->rateLimitQueue();
			$optional = [
				'dimensions' => 'ga:pagePath',
				'max-results' => intval($maxResults),
				'sort' => '-ga:pageviews'
			];
			if (!empty($filter)) {
				$optional['filters'] = $filter;
			}

			$gaData = $this->analytics->data_ga->get(
				'ga:'.$this->gaInfo[$siteKey]['profile_id'],
				gmdate('Y-m-d', $startTimestamp),
				gmdate('Y-m-d', $endTimestamp),
				implode(',', $this->metrics),
				$optional
			);

			if ($gaData instanceof \Google_Service_Analytics_GaData) {
				if (is_array($gaData->rows)) {
					foreach ($gaData->rows as $data) {
						$page = $data[0];
						$pageData[$this->metrics[0]][$page] = $data[1];
						$pageData[$this->metrics[1]][$page] = $data[2];
					}
				}
			}
		} catch (\Google_Service_Exception $e) {
			wfDebug("We could not retrieve popular pages data due to... ".$e->getMessage()."\n");
			return false;
		}

		return $pageData;
	}

	/**
	 * Get a list of top popular files.
	 *
	 * @access	public
	 * @param	integer	Start Timestamp - Unix Format
	 * @param	integer	End Timestamp - Unix Format
	 * @param	string	Site key to look up.
	 * @param	integer	[Optional] Maximum number of results.
	 * @return	mixed	Array of organized data or false on error.
	 */
	public function getPopularFiles($startTimestamp, $endTimestamp, $siteKey, $maxResults = 10) {
		global $wgContLang;

		return $this->getPopularPages($startTimestamp, $endTimestamp, $siteKey, $maxResults, 'ga:pagePath=@/'.$wgContLang->getNsText(NS_FILE).':');
	}

	/**
	 * Get an overall device breakdown.
	 *
	 * @access	public
	 * @param	integer	Start Timestamp - Unix Format
	 * @param	integer	End Timestamp - Unix Format
	 * @param	string	Site key to look up.
	 * @return	mixed	Array of organized data or false on error.
	 */
	public function getDeviceBreakdown($startTimestamp, $endTimestamp, $siteKey) {
		if ($this->validateParameters($startTimestamp, $endTimestamp, $siteKey) === false) {
			return false;
		}

		$breakdown = [];
		try {
			$this->rateLimitQueue();

			$dimensions = ['ga:browser', 'ga:deviceCategory'];
			foreach ($dimensions as $dimension) {
				$breakdown[$dimension] = [];
			}

			$gaData = $this->analytics->data_ga->get(
				'ga:'.$this->gaInfo[$siteKey]['profile_id'],
				gmdate('Y-m-d', $startTimestamp),
				gmdate('Y-m-d', $endTimestamp),
				'ga:sessions',
				['dimensions' => implode(',', $dimensions)]
			);

			if ($gaData instanceof \Google_Service_Analytics_GaData) {
				if (is_array($gaData->rows)) {
					foreach ($gaData->rows as $data) {
						if (!isset($breakdown[$dimensions[0]][$data[0]])) {
							$breakdown[$dimensions[0]][$data[0]] = 0;
						}
						if (!isset($breakdown[$dimensions[1]][$data[1]])) {
							$breakdown[$dimensions[1]][$data[1]] = 0;
						}
						$breakdown[$dimensions[0]][$data[0]] += $data[2];
						$breakdown[$dimensions[1]][$data[1]] += $data[2];
					}
				}
			}
		} catch (\Google_Service_Exception $e) {
			wfDebug("We could not retrieve browser breakdown data due to... ".$e->getMessage()."\n");
			return false;
		}

		return $breakdown;
	}

	/**
	 * Validates the basic GA parameters.
	 *
	 * @access	private
	 * @param	integer	Start Timestamp - Unix Format
	 * @param	integer	End Timestamp - Unix Format
	 * @param	string	Site key to look up.
	 * @return	boolean
	 */
	private function validateParameters($startTimestamp, $endTimestamp, $siteKey) {
		if (!isset($this->gaInfo[$siteKey]['view_id'])) {
			return false;
		}

		$startTimestamp = intval($startTimestamp);
		$endTimestamp = intval($endTimestamp);

		if ($startTimestamp == $endTimestamp) {
			return false;
		}
		return true;
	}

	/**
	 * Implements a rate limit queue for Google Analytic requests
	 *
	 * @access	public
	 * @return	void
	 */
	public function rateLimitQueue(){
		$listLength = $this->redis->lLen('hydralytics:gaRateLimitList');
		$timestamp = microtime(true);

		// If we are below 10 reqs in the queue, let's just push the latest req on top.
		if ($listLength >= 10) {
			// We need to compare the 10th element in the index to make sure we are staying in our 10reqs/second
			$oldestItem = $this->redis->lIndex('hydralytics:gaRateLimitList', 9);
			$timeDifference = $timestamp-$oldestItem;

			// If the time difference is less than 1 second, we need to sleep to get past it.
			if ($timeDifference < 1) {
				usleep($timeDifference * 1000001);
			}

			// Time to get rid of the oldest item and push the latest request onto the stack.
			$this->redis->rPop('hydralytics:gaRateLimitList');
			$this->redis->lPush('hydralytics:gaRateLimitList', $timestamp);
		} else {
			$this->redis->lPush('hydralytics:gaRateLimitList', $timestamp);
		}
	}
}
