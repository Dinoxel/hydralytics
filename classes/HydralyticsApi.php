<?php
/**
 * Curse Inc.
 * Hydralytics
 * Hydralytics API for ajax functions
 *
 * @author		Noah Manneschmidt
 * @copyright	(c) 2014 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		Hydralytics
 * @link		https://gitlab.com/hydrawiki
 *
 **/

class HydralyticsApi extends HydraApiBase {
	public function getDescription() {
		return 'Provides utility functions for Hydralytics ajax functionality';
	}

	public function getActions() {
		return [
			'dailyRefresh' => [
				'tokenRequired' => true,
				'postRequired' => true,
			],
			'dailyStatCheck' => [
				'tokenRequired' => false,
				'postRequired' => false,
			]
		];
	}

	/**
	 * Triggers a refresh of the information from doDailyStatCheck().
	 *
	 * @access	public
	 * @return	void
	 */
	public function doDailyRefresh() {
		global $wgUser;

		if (!$wgUser->isAllowed('ga_recache')) {
			$this->dieWithError(wfMessage('permissionserrors')->text(), 'no_permission', null, 403);
		}
		DailyAnalytics::queue();
		$redis = \RedisCache::getClient('cache');
		$redis->del('hydralytics:gaDailyData');
		$this->doDailyStatCheck();
	}

	public function doDailyStatCheck() {
		$redis = RedisCache::getClient('cache');
		$this->getResult()->addValue(null, 'timestamp', $redis->get('hydralytics:gaDailyDataUpdatedAt'));
		$this->getResult()->addValue(null, 'siteCount', $redis->hLen('hydralytics:gaDailyData'));
	}
}
